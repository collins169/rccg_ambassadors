import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rccg_ambassadors/blocs/register_bloc.dart';
import 'package:rccg_ambassadors/screens/base_view.dart';
import 'package:rccg_ambassadors/utils/app_state.dart';
import 'package:rccg_ambassadors/utils/constants.dart';
import 'package:rccg_ambassadors/utils/locator.dart';
import 'package:rccg_ambassadors/utils/urls.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  GlobalKey<FormState> _formKey;
  GlobalKey<ScaffoldState> _sKey;

  String _gender;

  List org;

  // dynamic _org;

  bool _inAsyncCall = false;

  TextEditingController _firstNameController;
  TextEditingController _surNameController;
  TextEditingController _phoneController;
  TextEditingController _fellowshipController;

  List _groups;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey();
    _sKey = GlobalKey();

    _firstNameController = TextEditingController();
    _surNameController = TextEditingController();
    _phoneController = TextEditingController();
    _fellowshipController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<RegisterBloc>(
      onModelReady: (bloc) {
        bloc.getGroups();
      },
      builder: (BuildContext context, RegisterBloc value, Widget child) {
        if (value.state == AppState.LOADED_SUCCESSFULLY) {
          if (_groups == null) {
            _groups = value.groups;
            print(_groups);
          }
        }

        return ModalProgressHUD(
          inAsyncCall: _inAsyncCall == true || value.state == AppState.LOADING,
          progressIndicator: buildAnimation(),
          child: SafeArea(
            child: Scaffold(
              key: _sKey,
              body: _groups == null ? Container() : _buildBody(),
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Container(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: true,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.3,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: Text(
                  'Welcome',
                  style:
                      GoogleFonts.montserrat(fontSize: ScreenUtil().setSp(48)),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: TextFormField(
                  textCapitalization: TextCapitalization.words,
                  controller: _firstNameController,
                  validator: (value) =>
                      value.isEmpty ? 'First name is required' : null,
                  style: GoogleFonts.lato(
                    fontSize: ScreenUtil().setSp(36),
                  ),
                  decoration: InputDecoration(
                    labelText: 'First Name',
                    labelStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(28),
                    ),
                    errorStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: TextFormField(
                  textCapitalization: TextCapitalization.words,
                  controller: _surNameController,
                  validator: (value) =>
                      value.isEmpty ? 'Surname is required' : null,
                  style: GoogleFonts.lato(
                    fontSize: ScreenUtil().setSp(36),
                  ),
                  decoration: InputDecoration(
                    labelText: 'Surname',
                    labelStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(28),
                    ),
                    errorStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: TextFormField(
                  controller: _phoneController,
                  keyboardType: TextInputType.number,
                  validator: (value) =>
                      value.isEmpty ? 'Phone number is required' : null,
                  style: GoogleFonts.lato(
                    fontSize: ScreenUtil().setSp(36),
                  ),
                  decoration: InputDecoration(
                    labelText: 'Phone number',
                    labelStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(28),
                    ),
                    errorStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(8.0),
                      child: Text(
                        'Gender',
                        style: GoogleFonts.lato(
                          fontSize: ScreenUtil().setSp(32),
                        ),
                      ),
                    ),
                    RadioListTile<String>(
                      title: Text(
                        'Male',
                        style: GoogleFonts.lato(
                          fontSize: ScreenUtil().setSp(28),
                        ),
                      ),
                      groupValue: _gender,
                      onChanged: (value) => setState(() {
                        _gender = value;
                      }),
                      value: 'M',
                    ),
                    RadioListTile<String>(
                      title: Text(
                        'Female',
                        style: GoogleFonts.lato(
                          fontSize: ScreenUtil().setSp(28),
                        ),
                      ),
                      groupValue: _gender,
                      onChanged: (value) => setState(() {
                        _gender = value;
                      }),
                      value: 'F',
                    ),
                  ],
                ),
              ),
              // Container(
              //   margin: EdgeInsets.all(16.0),
              //   child: TextFormField(
              //     controller: _fellowshipController,
              //     onTap: () {
              //       showBottomSheet();
              //     },
              //     style: GoogleFonts.lato(
              //       fontSize: ScreenUtil().setSp(36),
              //     ),
              //     decoration: InputDecoration(
              //       labelText: 'Fellowship',
              //       labelStyle: TextStyle(
              //         fontSize: ScreenUtil().setSp(28),
              //       ),
              //       errorStyle: TextStyle(
              //         fontSize: ScreenUtil().setSp(24),
              //       ),
              //       border: OutlineInputBorder(
              //         borderRadius: BorderRadius.circular(8.0),
              //       ),
              //     ),
              //   ),
              // ),
              Container(
                height: ScreenUtil().setHeight(100),
                margin: EdgeInsets.all(16.0),
                child: FlatButton(
                  child: Text(
                    'Register',
                    style: GoogleFonts.lato(
                        fontSize: ScreenUtil().setSp(28), color: Colors.white),
                  ),
                  color: Color(0xFF101260),
                  onPressed: () async {
                    await _validate();
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _validate() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    if (_gender == null) {
      _sKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Please select a gender'),
          backgroundColor: Colors.red,
        ),
      );
      return;
    }

    final model = {
      'first_name': _firstNameController.text,
      'surname': _surNameController.text,
      'phone': _phoneController.text,
      'gender': _gender,
      'group': 0,
      'dept': '',
    };

    print(model);

    _inAsyncCall = true;
    setState(() {});

    // Future.delayed(Duration(seconds: 5), () {

    // });

    final _rBloc = locator.get<RegisterBloc>();

    final result = await _rBloc.register(model);

    if (result) {
      setState(() {
        _inAsyncCall = false;
      });
      showDialog(
        context: context,
        builder: (context) {
          Future.delayed(Duration(seconds: 3), () {
            Navigator.of(context).pop(true);
            Navigator.pushNamed(context, SEARCH_PAGE_ROUTE);
          });
          return AlertDialog(
            content: Text(
              "Member Successfully Registered",
              style: GoogleFonts.montserrat(
                  fontSize: ScreenUtil().setSp(32), color: Color(0xFF101260)),
              textAlign: TextAlign.center,
            ),
            title: Container(
              height: ScreenUtil().setHeight(200),
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(16.0, 5.0, 16.0, 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/logo.png'),
                ),
              ),
            ),
          );
        },
      );
      // showSuccessDialog(
      //   'User created successfully',
      //   context,
      // );

      // Navigator.pushNamedAndRemoveUntil(
      //     context, SEARCH_PAGE_ROUTE, (_) => false);
    } else {
      setState(() {
        _inAsyncCall = false;
      });
      showErrorDialog(
          'Unable to register user. User may already exist', context);
    }
  }

  showBottomSheet() {
    return showModalBottomSheet(
      context: context,
      isDismissible: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      builder: (BuildContext context) {
        return Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8.0),
              child: Text(
                'Select a Fellowship',
                style: TextStyle(fontSize: 18),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _groups.length,
                itemBuilder: (BuildContext context, int index) {
                  var item = _groups[index];
                  return ListTile(
                    title: Text(item['name']),
                    onTap: () {
                      _fellowshipController.text = item['name'];
                      // _org = item;
                      Navigator.pop(context);
                      // FocusScope.of(context).unfocus();
                      setState(() {});
                    },
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }

  showErrorDialog(String errMsg, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0),
            ),
          ),
          title: Text('Error'),
          contentPadding: EdgeInsets.symmetric(vertical: 22, horizontal: 22),
          content: Text(errMsg),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Close',
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      },
    );
  }

  showSuccessDialog(String msg, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0),
            ),
          ),
          title: Text('Error'),
          contentPadding: EdgeInsets.symmetric(vertical: 22, horizontal: 22),
          content: Text(msg),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Close',
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
