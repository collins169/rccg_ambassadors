import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchResultWidget extends StatelessWidget {
  const SearchResultWidget({Key key, @required this.model, this.onTap})
      : super(key: key);
  final dynamic model;
  final VoidCallback onTap;

  // id
  // first_name
  // surname
  // phone
  // present
  // department

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(16.0),
      elevation: 4.0,
      child: Column(
        children: <Widget>[
          Container(
            child: ListTile(
              dense: true,
              title: ListTile(
                dense: true,
                leading: Container(
                  child: Icon(Icons.person),
                ),
                title: Container(
                  child: Text(
                    '${model['first_name']} ${model['surname']}',
                    style: GoogleFonts.montserrat(
                      fontSize: ScreenUtil().setSp(28),
                    ),
                  ),
                ),
              ),
              subtitle: Column(
                children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Container(
                      child: Icon(Icons.phone),
                    ),
                    title: Text(
                      '${model['phone']}',
                      style: GoogleFonts.montserrat(
                        fontSize: ScreenUtil().setSp(28),
                      ),
                    ),
                  ),
                  ListTile(
                    dense: true,
                    leading: Container(
                      child: Icon(Icons.business),
                    ),
                    title: Container(
                      child: Text(
                        '${model['department'] ?? 'N/A'}',
                        style: GoogleFonts.montserrat(
                          fontSize: ScreenUtil().setSp(28),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              trailing: IconButton(
                icon: Icon(
                  Icons.check_circle_outline,
                  color: model['present'] == 1 ? Colors.green : Colors.grey,
                  size: ScreenUtil().setHeight(48),
                ),
                onPressed: model['present'] == 1 ? null : onTap,
              ),
            ),
          ),
          Visibility(
            visible: model['present'] != 1,
            child: Container(
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Image.asset('assets/images/swipe.gif'),
            ),
          ),
          Visibility(
            visible: model['present'] != 1,
            child: Container(
              margin: EdgeInsets.all(4.0),
              child: Text('Swipe to mark as present'),
            ),
          ),
        ],
      ),
    );
  }
}
