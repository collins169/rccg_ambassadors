import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rccg_ambassadors/blocs/blocs.dart';
import 'package:rccg_ambassadors/screens/base_view.dart';
import 'package:rccg_ambassadors/screens/search_page/search_result_widget.dart';
import 'package:rccg_ambassadors/utils/app_state.dart';
import 'package:rccg_ambassadors/utils/constants.dart';
import 'package:rccg_ambassadors/utils/locator.dart';
import 'package:rccg_ambassadors/utils/urls.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  GlobalKey<FormState> _formKey;
  GlobalKey<ScaffoldState> _skey;

  List data;

  FocusNode _searchNode;

  TextEditingController _searchController;
  int dataLength;

  bool _inAsyncCall = false;

  @override
  void initState() {
    super.initState();
    _skey = GlobalKey();
    _formKey = GlobalKey();
    _searchController = TextEditingController();
    _searchNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<SearchBloc>(
      builder: (BuildContext context, SearchBloc value, Widget child) {
        if (value.state == AppState.LOADED_SUCCESSFULLY) {
          data = value.data;
          print(data);

          data.sort((a, b) => a['present'] < b['present']);
          dataLength = data.length;
          setState(() {});
        }
        return ModalProgressHUD(
          inAsyncCall: _inAsyncCall,
          progressIndicator: buildAnimation(),
          child: Scaffold(
            key: _skey,
            body: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Container(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  margin: EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 16.0),
                  child: Form(
                    autovalidate: true,
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(16.0),
                          child: Center(
                            child: Wrap(
                              // crossAxisAlignment:,
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  'assets/images/logo.png',
                                  height: ScreenUtil().setHeight(100),
                                ),
                                Container(
                                  margin: EdgeInsets.all(8.0),
                                  child: Text(
                                    'Welcome to Church',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.montserrat(
                                      fontSize: ScreenUtil().setSp(
                                        50,
                                      ),
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xFF101260),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(16.0),
                          child: TextFormField(
                            focusNode: _searchNode,
                            controller: _searchController,
                            keyboardType: TextInputType.number,
                            validator: (value) => value.isEmpty
                                ? 'Please enter phone number'
                                : null,
                            onChanged: (value) => _validate2(value),
                            style: GoogleFonts.lato(
                              fontSize: ScreenUtil().setSp(32),
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              labelText: 'Please type your phone number',
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(32),
                              ),
                              errorStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(32),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(16.0),
                          height: ScreenUtil().setHeight(100),
                          child: FlatButton(
                            child: Text(
                              'Search',
                              style: GoogleFonts.lato(
                                  fontSize: ScreenUtil().setSp(28),
                                  color: Colors.white),
                            ),
                            color: Color(0xFF101260),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            onPressed: () {
                              _validate();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          data == null
              ? SliverList(
                  delegate: SliverChildListDelegate(
                    [],
                  ),
                )
              : data.length < 1
                  ? SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Container(
                            margin: EdgeInsets.all(16.0),
                            child: Image.asset(
                              'assets/images/not_found.png',
                              height: ScreenUtil().setHeight(500),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.all(16.0),
                            child: Text(
                              'Member not found.',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.montserrat(
                                fontSize: ScreenUtil().setSp(30),
                              ),
                            ),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(100),
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.all(16.0),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              color: Colors.green,
                              materialTapTargetSize:
                                  MaterialTapTargetSize.padded,
                              child: Text(
                                'Register',
                                style: GoogleFonts.montserrat(
                                    fontSize: ScreenUtil().setSp(32),
                                    color: Colors.white),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, REGISTER_PAGE_ROUTE);
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  : SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          var item = data[index];
                          return item['present'] == 0
                              ? Dismissible(
                                  child: SearchResultWidget(
                                    model: item,
                                  ),
                                  key: UniqueKey(),
                                  onDismissed: (_) {
                                    if (item['present'] >= 1) {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          Future.delayed(Duration(seconds: 3),
                                              () {
                                            Navigator.of(context).pop(true);
                                            Navigator.pushNamed(
                                                context, SEARCH_PAGE_ROUTE);
                                          });
                                          return AlertDialog(
                                            content: Text(
                                              "Sorry ${item['first_name']} you've been marked already",
                                              style: GoogleFonts.montserrat(
                                                  fontSize:
                                                      ScreenUtil().setSp(32),
                                                  color: Color(0xFF101260)),
                                              textAlign: TextAlign.center,
                                            ),
                                            title: Container(
                                              height:
                                                  ScreenUtil().setHeight(200),
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: EdgeInsets.fromLTRB(
                                                  16.0, 5.0, 16.0, 8.0),
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/logo.png'),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    } else {
                                      _markAsPresent2(
                                          item['phone'], item['first_name']);
                                    }
                                    data.removeWhere((test) =>
                                        test['phone'] == item['phone']);

                                    print(data.contains(item));
                                    if (dataLength == 1) {
                                      _searchController.text = '';
                                      data = null;
                                    }
                                    Navigator.pushNamed(
                                        context, SEARCH_PAGE_ROUTE);
                                  },
                                )
                              : GestureDetector(
                                  onTap: () {
                                    // _markAsPresent(item['phone']);
                                    if (item['present'] >= 1) {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          Future.delayed(Duration(seconds: 3),
                                              () {
                                            Navigator.of(context).pop(true);
                                            Navigator.pushNamed(
                                                context, SEARCH_PAGE_ROUTE);
                                          });
                                          return AlertDialog(
                                            content: Text(
                                              "Sorry ${item['first_name']} you've been marked already",
                                              style: GoogleFonts.montserrat(
                                                  fontSize:
                                                      ScreenUtil().setSp(32),
                                                  color: Color(0xFF101260)),
                                              textAlign: TextAlign.center,
                                            ),
                                            title: Container(
                                              height:
                                                  ScreenUtil().setHeight(200),
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: EdgeInsets.fromLTRB(
                                                  16.0, 5.0, 16.0, 8.0),
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/logo.png'),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    } else {
                                      _markAsPresent2(
                                          item['phone'], item['first_name']);
                                    }
                                    // Navigator.pushNamed(
                                    // context, SEARCH_PAGE_ROUTE);
                                  },
                                  child: SearchResultWidget(
                                    model: item,
                                    onTap: () {
                                      // _markAsPresent(item['phone']);
                                      Navigator.pushNamed(
                                          context, SEARCH_PAGE_ROUTE);
                                    },
                                  ),
                                );
                        },
                        childCount: data.length,
                      ),
                    ),
        ],
      ),
    );
  }

  _validate() async {
    final searchBloc = locator.get<SearchBloc>();

    if (!_formKey.currentState.validate()) {
      return;
    }

    FocusScope.of(context).unfocus();

    setState(() {
      _inAsyncCall = true;
    });
    final result = await searchBloc.searchUser(_searchController.text.trim());

    if (result) {
      data = searchBloc.data;
    }
    _inAsyncCall = false;
    setState(() {});
  }

  _validate2(String text) async {
    if ((text.length == 10 || text.length == 13)) {
      final searchBloc = locator.get<SearchBloc>();

      FocusScope.of(context).unfocus();

      setState(() {
        _inAsyncCall = true;
      });

      final result = await searchBloc.searchUser(_searchController.text.trim());
      if (result) {
        data = searchBloc.data;
        print(data);
        // if (data.length > 0) {
        //   _skey.currentState.showSnackBar(
        //     SnackBar(
        //       content: Text(
        //         'Please swipe to mark',
        //         style: TextStyle(color: Colors.white),
        //       ),
        //       backgroundColor: Colors.blue,
        //       duration: Duration(seconds: 5),
        //     ),
        //   );
        //   showDialog(
        //       context: context,
        //       builder: (context) {
        //         Future.delayed(Duration(seconds: 2), () {
        //           Navigator.of(context).pop(true);
        //         });
        //         return AlertDialog(
        //           content: Text(
        //             "Please Swiper left or right to mark",
        //             style: GoogleFonts.montserrat(
        //                 fontSize: ScreenUtil().setSp(32),
        //                 color: Color(0xFF101260)),
        //             textAlign: TextAlign.center,
        //           ),
        //           title: Container(
        //               height: ScreenUtil().setHeight(200),
        //               width: MediaQuery.of(context).size.width,
        //               margin: EdgeInsets.fromLTRB(16.0, 5.0, 16.0, 8.0),
        //               decoration: BoxDecoration(
        //                 image: DecorationImage(
        //                   image: AssetImage('assets/images/swipe.png'),
        //                 ),
        //               )),
        //         );
        //       });
        // }
      }
      _inAsyncCall = false;
      setState(() {});
    }
  }

  _markAsPresent2(String phoneNumber, String name) async {
    final searchBloc = locator.get<SearchBloc>();

    setState(() {
      _inAsyncCall = true;
    });

    final result = await searchBloc.markPresent(phoneNumber);

    if (result) {
      showDialog(
        context: context,
        builder: (context) {
          Future.delayed(
            Duration(seconds: 3),
            () {
              Navigator.of(context).pop(true);
              Navigator.pushNamed(context, SEARCH_PAGE_ROUTE);
            },
          );
          return AlertDialog(
            content: Text(
              "Welcome to Church $name",
              style: GoogleFonts.montserrat(
                  fontSize: ScreenUtil().setSp(32), color: Color(0xFF101260)),
              textAlign: TextAlign.center,
            ),
            title: Container(
              height: ScreenUtil().setHeight(200),
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(16.0, 5.0, 16.0, 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/logo.png'),
                ),
              ),
            ),
          );
        },
      );

      _validate();
    }

    setState(() {
      _inAsyncCall = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
