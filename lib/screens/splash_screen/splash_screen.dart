import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rccg_ambassadors/utils/urls.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    startTimeout();
  }

  startTimeout() {
    Timer(Duration(milliseconds: 2000), goToHome);
  }

  goToHome() {
    Navigator.pushReplacementNamed(context, SEARCH_PAGE_ROUTE);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);

    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage('assets/images/logo.png'),
          fit: BoxFit.contain,
        ),
      ),
      // child: Center(
      //   child: Container(
      //     margin: EdgeInsets.all(32.0),
      //     // child: Text(
      //     //   '',
      //     //   textAlign: TextAlign.start,
      //     //   style: GoogleFonts.montserrat(
      //     //     textStyle: TextStyle(fontSize: 32),
      //     //   ),
      //     // ),
      //   ),
      // ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
