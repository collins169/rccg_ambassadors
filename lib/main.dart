import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rccg_ambassadors/app.dart';
import 'package:rccg_ambassadors/utils/locator.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  setupLocators();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    runApp(RccgApp());
  });
}
