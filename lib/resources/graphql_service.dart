import 'package:graphql/client.dart';
import 'package:rccg_ambassadors/utils/urls.dart';

class GraphQLService {
  GraphQLClient _client;

  HttpLink _http;
  _init() async {
    _http =
        HttpLink(uri: BASE_URL, headers: {'Content-Type': 'application/json'});

    Link link = _http;

    _client = GraphQLClient(link: link, cache: InMemoryCache());
  }

  GraphQLService();

  Future<QueryResult> makeQuery(String query, {dynamic variables}) async {
    await _init();

    QueryOptions options = QueryOptions(document: query, variables: variables);

    final result = await _client.query(options).timeout(Duration(minutes: 2));

    return result;
  }

  Future<QueryResult> performMutation(String query, {dynamic variables}) async {
    await _init();

    MutationOptions options =
        MutationOptions(document: query, variables: variables);

    final result = await _client.mutate(options).timeout(Duration(minutes: 2));

    return result;
  }
}
