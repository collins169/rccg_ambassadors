import 'package:rccg_ambassadors/blocs/base_bloc.dart';
import 'package:rccg_ambassadors/resources/graphql_service.dart';
import 'package:rccg_ambassadors/utils/app_state.dart';
import 'package:rccg_ambassadors/utils/locator.dart';

class RegisterBloc extends BaseBloc {
  GraphQLService _service;

  RegisterBloc() {
    _service = locator.get<GraphQLService>();
  }

  List<dynamic> get groups => _groups;

  List<dynamic> _groups;

  Future<bool> register(dynamic input) async {
    setState(AppState.LOADING);
    String query = r'''
      mutation($input: addMember) {
        addMember(type: $input)
      }
    ''';

    Map<String, dynamic> variables = {'input': input};

    try {
      final result =
          await _service.performMutation(query, variables: variables);

      print(result.data);
      print(result.errors);

      if (result.hasErrors) {
        setState(AppState.LOADED_WITH_ERROR);
        return false;
      } else {
        setState(AppState.LOADED_SUCCESSFULLY);
        return true;
      }
    } catch (e) {
      print(e);
      setState(AppState.LOADED_WITH_ERROR);
      return false;
    }
  }

  Future<bool> getGroups() async {
    setState(AppState.LOADING);

    String query = r'''
      query {
        groups {
          id
          name
        }
      }
    ''';

    try {
      final result = await _service.performMutation(query);

      if (result.hasErrors) {
        setState(AppState.LOADED_WITH_ERROR);
        return false;
      } else {
        setState(AppState.LOADED_SUCCESSFULLY);
        _groups = result.data['groups'];
        return true;
      }
    } catch (e) {
      print(e);
      setState(AppState.LOADED_WITH_ERROR);
      return false;
    }
  }
}
