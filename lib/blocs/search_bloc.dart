import 'package:rccg_ambassadors/blocs/base_bloc.dart';
import 'package:rccg_ambassadors/resources/graphql_service.dart';
import 'package:rccg_ambassadors/utils/app_state.dart';
import 'package:rccg_ambassadors/utils/locator.dart';

class SearchBloc extends BaseBloc {
  GraphQLService _service;

  dynamic get data => _data;
  dynamic get errors => _error;

  dynamic _data;
  dynamic _error;

  SearchBloc() {
    _service = locator.get<GraphQLService>();
  }

  Future<bool> searchUser(String queryParam) async {
    setState(AppState.LOADING);
    String query = r'''
      query($queryParam: String!) {
        search(param: $queryParam) {
          id
          first_name
          surname
          phone 
          present
          department
        }
      }
    ''';

    Map<String, dynamic> variables = {'queryParam': queryParam};

    try {
      final result = await _service.makeQuery(query, variables: variables);

      if (result.hasErrors) {
        setState(AppState.LOADED_WITH_ERROR);
        _error = result.errors.toString();
        print(result.errors);
        return false;
      } else {
        print(result.data);
        setState(AppState.LOADED_SUCCESSFULLY);
        _data = result.data['search'];
        return true;
      }
    } catch (e) {
      print(e);
      _error = e.toString();
      setState(AppState.LOADED_WITH_ERROR);
      return false;
    }
  }

  Future<bool> markPresent(String phoneNumber) async {
    setState(AppState.LOADING);

    String query = r'''
      mutation($phone: String!) {
        markAttendances(phone: $phone)
      }
    ''';

    final variables = {'phone': phoneNumber};

    try {
      final result =
          await _service.performMutation(query, variables: variables);

      if (result.hasErrors) {
        setState(AppState.LOADED_WITH_ERROR);
        print(result.errors);
        return false;
      } else {
        setState(AppState.LOADED_SUCCESSFULLY);
        print(result.data);
        return true;
      }
    } catch (e) {
      print(e);
      setState(AppState.LOADED_WITH_ERROR);
      return false;
    }
  }
}
