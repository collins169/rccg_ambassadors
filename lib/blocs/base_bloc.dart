import 'package:flutter/foundation.dart';
import 'package:rccg_ambassadors/utils/app_state.dart';

abstract class BaseBloc with ChangeNotifier {
  AppState _state;

  AppState get state => _state;

  BaseBloc() {
    _state = AppState.IDLE;
  }

  setState(AppState state) {
    _state = state;
    notifyListeners();
  }
}
