import 'package:flutter/material.dart';
import 'package:rccg_ambassadors/utils/routes.dart';

class RccgApp extends StatefulWidget {
  RccgApp({Key key}) : super(key: key);

  @override
  _RccgAppState createState() => _RccgAppState();
}

class _RccgAppState extends State<RccgApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RCCG Ambassadors',
      onGenerateRoute: Routes.routes(),
      debugShowCheckedModeBanner: false,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
