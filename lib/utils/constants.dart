import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_animations/simple_animations.dart';

final tween = MultiTrackTween([
  Track("opacity")
      .add(Duration(milliseconds: 600), Tween(begin: 0.0, end: 1.0)),
]);

Widget buildAnimation() {
  return ControlledAnimation(
    playback: Playback.MIRROR,
    duration: tween.duration,
    tween: tween,
    builder: (context, animation) {
      return Center(
        child: Container(
          child: Opacity(
            opacity: animation['opacity'],
            child: Image(
              image: AssetImage('assets/images/logo.png'),
              height: ScreenUtil().setHeight(300),
            ),
          ),
        ),
      );
    },
  );
}
