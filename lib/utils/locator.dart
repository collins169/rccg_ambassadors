import 'package:get_it/get_it.dart';
import 'package:rccg_ambassadors/blocs/blocs.dart';
import 'package:rccg_ambassadors/resources/graphql_service.dart';

GetIt locator = GetIt.I;

void setupLocators() {
  locator.registerFactory<GraphQLService>(() => GraphQLService());

  locator.registerFactory<SearchBloc>(() => SearchBloc());

  locator.registerFactory<RegisterBloc>(() => RegisterBloc());
}
