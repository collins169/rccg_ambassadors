import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rccg_ambassadors/screens/screens.dart';
import 'package:rccg_ambassadors/utils/urls.dart';

class Routes {
  static RouteFactory routes() {
    return (RouteSettings settings) {
      // var args = settings.arguments;
      final name = settings.name;
      Widget screen;

      switch (name) {
        case SPLASH_SCREEN_ROUTE:
          screen = SplashScreen();
          break;
        case SEARCH_PAGE_ROUTE:
          screen = SearchPage();
          break;
        case REGISTER_PAGE_ROUTE:
          screen = RegisterScreen();
          break;
        default:
          screen = SearchPage();
      }

      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}
